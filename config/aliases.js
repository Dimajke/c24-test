const path = require('path');

const resolve = pathToFile => path.join(__dirname, pathToFile);
const aliases = {
  '@components': resolve('../components'),
};

module.exports = aliases;
