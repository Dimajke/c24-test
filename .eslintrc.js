module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
  },
  extends: [
    'airbnb-base',
    '@nuxtjs',
    'plugin:nuxt/recommended',
  ],
  plugins: [
  ],
  rules: {
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'state',
        ],
      },
    ],
    'operator-linebreak': [2, 'before'],
    'space-before-function-paren': [
      'error',
      'never',
    ],
    semi: ['error', 'always'],
    'linebreak-style': 0,
    quotes: ['error', 'single', 'avoid-escape'],
    'comma-dangle': ['error', 'always-multiline'],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        vue: 'never',
        js: 'never',
      },
    ],
    'no-shadow': [
      'error',
      {
        allow: [
          'state',
          'Vue',
        ],
      },
    ],
    'vue/mustache-interpolation-spacing': ['error', 'never'],
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'arrow-parens': ['error', 'as-needed'],
    curly: 'off',
    'no-console': 'off',
    'vue/no-v-html': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-underscore-dangle': 'off',
  },
  settings: {
    'import/extensions': ['.js', '.vue'],
    'import/resolver': {
      alias: {
        map: [
          ['~', './'],
        ],
        extensions: ['.js', '.vue', '.json', '.scss'],
      },
      node: {
        extensions: ['.js', '.vue'],
      },
    },
  },
};
