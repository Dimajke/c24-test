const IMAGES_DIR = '/images/cars';

export default [
  {
    id: '1',
    title: 'Sunlight A70 3.5t',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/1.webp`,
  },
  {
    id: '2',
    title: ' Malibu T500 QB 9G Tronic 4.2 ',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/2.webp`,
  },
  {
    id: '3',
    title: ' Westfalia Club Joker City Al ',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/3.webp`,
  },
  {
    id: '4',
    title: ' Globecar Globescout Plus 3.5',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/4.webp`,
  },
  {
    id: '5',
    title: ' HYMER / ERIBA / HYMERCAR Nov... ',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/5.webp`,
  },
  {
    id: '6',
    title: ' Frankia T 700 BD Comfort Aut... ',
    timeEnd: '1682421235',
    imageSrc: `${IMAGES_DIR}/6.webp`,
  },
];
