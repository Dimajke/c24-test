import auctions from '~/content/auctions';

export const state = () => ({
  auctionsList: [],
});

export const getters = {
  getAuctionsList: state => state.auctionsList,
};

export const mutations = {
  setAuctions(state, payload) {
    state.auctionsList = payload;
  },
};

export const actions = {
  loadAuctions({ commit }, payload) {
    commit('setAuctions', auctions);
  },
};
