// eslint-disable-next-line nuxt/no-cjs-in-config
const aliases = require('./config/aliases');

export default {
  head: {
    title: 'c24-test',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  css: [
    '~/assets/scss/global.scss',
    'vue-slick-carousel/dist/vue-slick-carousel.css',
  ],

  plugins: [
    { src: './plugins/vue-slick-carousel.js' },
  ],

  components: false,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxtjs/style-resources',
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxt/image',
  ],

  styleResources: {
    scss: [
      './assets/scss/_variables.scss',
    ],
  },

  axios: {
    baseURL: '/',
  },

  build: {
    extend(config, ctx) {
      // eslint-disable-next-line no-param-reassign
      config.resolve.alias = {
        ...config.resolve.alias,
        ...aliases,
      };
    },
  },
};
