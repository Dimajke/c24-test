const menu = [
  {
    id: 'kaufen',
    link: 'kaufen',
    title: 'Kaufen',
  },
  {
    id: 'verkaufen',
    link: 'verkaufen',
    title: 'Verkaufen',
  },
  {
    id: 'login',
    link: 'login',
    title: 'Login',
  },
];

export default menu;
